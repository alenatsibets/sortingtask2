public class SortingTask2 {
    public static void main(java.lang.String[] args) {
        String[] array = {"Ivanov", "Petrov", "Abikenov", "Ivanov", "Egorov", "Minin"};
        display("Unsorted array:", array);
        selectionSort(array);
        display("Selection sorting:", array);

        String[] array2 = {"Ivanov", "Petrov", "Abikenov", "Ivanov", "Egorov", "Minin"};
        bubbleSort(array2);
        display("Bubble sorting:", array2);

        String[] array3 = {"Ivanov", "Petrov", "Abikenov", "Ivanov", "Egorov", "Minin"};
        insertionSort(array3);
        display("Insertion sorting:", array3);

    }
    public static void selectionSort(String[] arr) {
        int pos;
        String temp;
        for (int i = 0; i < arr.length; i++) {
            pos = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j].compareTo(arr[pos])<0) {
                    pos = j;
                }
            }

            temp = arr[pos];
            arr[pos] = arr[i];
            arr[i] = temp;
        }
    }

    public static void bubbleSort(String[] arr) {

        boolean isSorted = false;
        String buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length-1; i++) {
                if(arr[i].compareTo(arr[i+1]) > 0){
                    isSorted = false;

                    buf = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = buf;
                }
            }
        }
    }

    public static void insertionSort(String[] arr) {
        for (int i = 1; i < arr.length; i++) {
            String currElem = arr[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && arr[prevKey].compareTo(currElem) > 0) {
                arr[prevKey + 1] = arr[prevKey];
                arr[prevKey] = currElem;
                prevKey--;
            }
        }
    }

    public static void display(String message, String[] arr){
        System.out.println(message);
        for (String element : arr) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}